# newchat

## Development

```sh
npm i
npx tailwindcss -i style/base.css -o style/main.css --watch
npx -y http-server .
```

## Deploy

The deployment of this site is managed by CloudFlare Pages through GitLab.

## Libraries used

- https://github.com/jackocnr/intl-tel-input
- https://github.com/argyleink/gui-challenges/tree/main/toast

## TODO

- Support dark mode
- Toggle and remember WhatsApp/WhatsApp Business
- Use IP to set default country
- Remember country code
- Support broadcast lists
