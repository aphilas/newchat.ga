const phoneInputField = document.querySelector("#phone")

const phoneInput = window.intlTelInput(phoneInputField, {
  preferredCountries: ["ke", "us", "in"],
  utilsScript:
    "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
})

/**
 * Start a WhatsApp chat
 * @param {string} phoneNumber Number in the standardized format 
 */
function startChat(phoneNumber) {
  window.location.href = `https://wa.me/${phoneNumber.slice(1, phoneNumber.length)}`
}

function submitNumber(event) {
  if (event) {
    event.preventDefault()
  }

  /**
   * @type {string}
   */
  const phoneNumber = phoneInput.getNumber()

  if (phoneInput.isValidNumber()) {
    startChat(phoneNumber)
  } else {
    Toast("Invalid phone number")
  }
}

const contactsApiSupported = 'contacts' in navigator && 'ContactsManager' in window

/**
 * @returns {Promise<{'tel': string[]}[]>}
 */
async function getContacts() {
  const props = ['tel']
  const opts = {multiple: false}

  try {
    return await navigator.contacts.select(props, opts)
  } catch (error) {
    return
  }
}

const contactsSelect = document.querySelector("#contacts-select")
const contactsModalInput = document.querySelector("#contact-modal")
const chatForm = document.querySelector("#chat")

const delay = timeout => new Promise((resolve) => {
  setTimeout(resolve, timeout)
})

/**
 * 
 * @param {Event} event 
 * @returns 
 */
async function pickContact(event) {
  if (!contactsApiSupported) {
    Toast("Contacts API is not supported")
    return
  }

  const contacts = await getContacts()
  // Toast(JSON.stringify(contacts))

  if (!contacts || contacts.length < 1) {
    Toast("No contacts selected")
    return
  }
  
  if (contacts.length > 1) {
    Toast("Something went wrong: multiple contacts selected")
    return
  }

  const contact = contacts[0]

  if (contact.tel.length == 1) {
    const number = contacts[0].tel[0]
    phoneInput.setNumber(number)
    submitNumber()
    return
  } 

  // Handle contacts with multiple numbers 

  const fragment = new DocumentFragment()
  for (const [i, n] of contact.tel.entries()){
    const option = document.createElement("option")
    option.value = n
    option.textContent = n

    if (i == 0) {
      option.selected = true
    }

    fragment.append(option)
  }

  contactsSelect.textContent = ""
  contactsSelect.append(fragment)

  // Show modal to select contacts
  contactsModalInput.checked = true
}

function selectContact(event) {
  event.preventDefault()
  phoneInput.setNumber(contactsSelect.value)
  submitNumber()
  contactsModalInput.checked = false
}

const contactsButton = document.querySelector("#contacts")

function onLoad() {
  if (contactsApiSupported) {
    contactsButton.classList.remove('btn-disabled')
  }
  
  if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
  }
}

window.addEventListener("load", onLoad)
