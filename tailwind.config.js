/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["**/*.{html,js}"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        "cupcake-green": {
          ...require("daisyui/src/colors/themes")["[data-theme=light]"],
          primary: "#43a047",
        },
      },
      "dark",
      "cupcake",
    ],
  },
}
